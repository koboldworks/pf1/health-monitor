const CFG = {
	module: 'koboldworks-pf1-health-monitor',
	SETTINGS: {
		transparencyFlag: 'unownedActor',
	},
	COLORS: {
		main: 'color:mediumseagreen',
		number: 'color:darkseagreen',
		unset: 'color:unset',
	},
	debug: false,
};

const hpKeys = ['offset', 'temp', 'nonlethal'];
const hpKeyMap = {
	value: 'hp',
	offset: 'hp',
	nonlethal: 'nl',
	temp: 'thp',
};

const wndsKeys = ['offset'];
const wndsKeyMap = {
	value: 'w',
	offset: 'w',
};

const vigKeys = ['offset', 'temp'];
const vigKeyMap = {
	value: 'v',
	offset: 'v',
	temp: 'vt',
};

const ablKeys = ['str', 'dex', 'con', 'int', 'wis', 'cha'];
const ablSubKeys = ['damage', 'drain', 'userPenalty'];
const correctAblSubKey = (d) => d === 'userPenalty' ? 'penalty' : d;

const deltaConfig = {
	hp: { sort: 2 }, // HP
	thp: { sort: 1 }, // Temp HP
	nl: { sort: 3, invert: true, suffer: true }, // Nonlethal
	ld: { sort: 10, invert: true, suffer: true }, // Level Drain
	v: { sort: 2 }, // Vigor
	vt: { sort: 1 }, // Temp Vigor
	w: { sort: 3 }, // Wounds
	str: { sort: 50 + 1, suffer: true, invert: true },
	dex: { sort: 50 + 2, suffer: true, invert: true },
	con: { sort: 50 + 3, suffer: true, invert: true },
	int: { sort: 50 + 4, suffer: true, invert: true },
	wis: { sort: 50 + 5, suffer: true, invert: true },
	cha: { sort: 50 + 6, suffer: true, invert: true },
};

class Delta {
	new = 0;
	old = 0;

	get heal() { return this.invert ? this.delta <= 0 : this.delta >= 0; }

	get delta() { return this.new - this.old; }

	constructor(newValue, oldValue, type, subKey) {
		this.new = newValue ?? 0;
		this.old = oldValue ?? 0;

		const _type = hpKeyMap[type] ?? type;
		this.type = _type;

		const cfg = deltaConfig[this.type];
		if (!cfg) console.warn('Missing type:', _type);

		Object.defineProperties(this, {
			invert: { value: !!cfg.invert },
			suffer: { value: !!cfg.suffer },
			sort: { value: cfg.sort },
		});

		if (ablKeys.includes(_type))
			this.subType = correctAblSubKey(subKey);
	}
}

/**
 * @param {Actor|Item} thing
 * @param {string | number} permission
 * @returns {User[]}
 */
const getUsers = (thing, permission) => Object
	.entries(thing.ownership)
	.filter(u => u[1] >= permission)
	.map(u => game.users.get(u[0]))
	.filter(u => u != undefined);

const getLabel = (type, subType) => {
	switch (type) {
		case 'hp': return game.i18n.localize('PF1.HitPoints');
		case 'thp': return game.i18n.localize('Koboldworks.HealthMonitor.Extend.TempHP');
		case 'nl': return game.i18n.localize('PF1.Nonlethal');
		case 'ld': return game.i18n.localize('PF1.LevelDrain');
		case 'str':
		case 'dex':
		case 'con':
		case 'int':
		case 'wis':
		case 'cha':
			return game.i18n.localize(`PF1.AbilityShort${type.capitalize()}`) + ' ' + game.i18n.localize(`PF1.${subType.capitalize()}`);
		case 'v': return game.i18n.localize('PF1.Vigor');
		case 'vt': return game.i18n.localize('Koboldworks.HealthMonitor.Extend.TempVigor');
		case 'w': return game.i18n.localize('PF1.Wounds');
		default: return type;
	}
};

/**
 * @param {Actor} actor
 * @param {object} ud
 */
async function printHealthCard(actor, ud) {
	/** @type {Delta[]} */
	const deltas = ud._all.filter(d => d.delta !== 0);

	const token = actor.token ?? actor.getActiveTokens(true, true)[0],
		scene = token?.parent;

	deltas.sort((d1, d2) => d1.sort - d2.sort);

	// TODO: Debounce collator with about 200ms + latency
	if (deltas.length === 0) return;

	const rollModeSwitch = {
		get rollmode() {
			let mode = game.settings.get('core', 'rollMode');
			if (mode === 'selfroll' && !game.user.isGM) mode = 'gmroll';
			else if (mode === 'blindroll') mode = !game.user.isGM ? 'gmroll' : 'selfroll';
			return mode;
		},
		get secret() { return game.user.isGM ? 'selfroll' : 'gmroll'; },
		get owned() { return actor.hasPlayerOwner ? 'roll' : game.user.isGM ? 'selfroll' : 'gmroll'; },
		get observer() {
			const defaultLevel = actor.testUserPermission('default', 'OBSERVER');
			return defaultLevel || getUsers(actor, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER).filter(u => !u.isGM).length ? 'roll' : 'gmroll';
		},
		get public() { return 'roll'; },
	};

	const tp = game.settings.get(CFG.module, CFG.SETTINGS.transparencyFlag);
	const rollMode = rollModeSwitch[tp] ?? rollModeSwitch.public;

	const whisper = [];
	if (['gmroll', 'selfroll'].includes(rollMode))
		whisper.push(...game.users.filter(u => u.isGM || u.id === game.user.id));

	const lang = {
		lose: 'Koboldworks.HealthMonitor.Card.Lose',
		gain: 'Koboldworks.HealthMonitor.Card.Gain',
		suffer: 'Koboldworks.HealthMonitor.Card.Suffer',
		buffer: 'Koboldworks.HealthMonitor.Card.Buffer',
	};

	const speaker = ChatMessage.getSpeaker({ actor, scene, token });
	if (!speaker.actor) return void console.error('Could not locate actor');
	// Set alias explicitly
	speaker.alias = token?.name || actor.name;

	const messages = [];
	const prelude = game.i18n.localize('Koboldworks.HealthMonitor.Card.Prelude');

	for (const d of deltas) {
		const value = d.delta,
			change = game.i18n.localize(d.heal ? d.suffer ? lang.lose : lang.gain : d.suffer ? lang.suffer : lang.lose);

		const target = getLabel(d.type, d.subType);

		const flags = {};
		flags[CFG.module] = {
			heal: d.heal,
			target: d.type,
			value: d.value,
		};
		if (d.subType) flags[CFG.module].subTarget = d.subType;

		const msgData = {
			content: `<div class="koboldworks health-monitor"><span class="prelude">${prelude}</span> <span class="label">${change}</span> <span class="value">${Math.abs(value)}</span> <span label="target">${target}</span></div>`,
			rollMode,
			whisper,
			speaker,
			flags,
			user: game.user.id,
			// type: CONST.CHAT_MESSAGE_TYPES.OOC
		};

		messages.push(msgData);
	}

	const hpd = deltas.find(d => d.type === 'hp');
	if (hpd) {
		const d = hpd.delta;
		const mDmg = game.settings.get(CFG.module, 'massiveDamage');
		if (mDmg && d <= -50 && d <= -Math.floor(actor.system.attributes.hp.max / 2)) {
			const change = game.i18n.localize(lang.suffer);
			const msgData = {
				content: `<div class="koboldworks health-monitor info"><span class="prelude">${prelude}</span> <span class="label">${change}</span> <span class="info">Massive Damage!</span></div>`,
				rollMode,
				whisper,
				speaker,
				flags: { [CFG.module]: { massiveDamage: true } },
				user: game.user.id,
				// type: CONST.CHAT_MESSAGE_TYPES.OOC
			};
			messages.push(msgData);
		}
	}

	if (messages.length) {
		return ChatMessage.create(messages);
	}
}

const harvestData = (oldD, newD, origin = {}) => {
	const ud = { _all: [] };

	if (!foundry.utils.isEmpty(origin)) oldD = mergeObject(origin, oldD, { overwrite: true, inplace: false });
	if (newD === undefined || oldD === undefined) return ud;
	newD = duplicate(newD ?? {}), oldD = duplicate(oldD ?? {});

	const nAttr = newD?.attributes, oAttr = oldD?.attributes;

	if (nAttr?.hp) {
		const hp = { _all: [] };
		for (const h of hpKeys) {
			if (h.startsWith('-=')) continue; // skip key deletion
			const value = nAttr.hp[h];
			if (value === undefined) continue;
			const d = new Delta(value, oAttr.hp[h], h);
			if (hp[d.type]) continue;
			hp[d.type] = d;
			ud._all.push(d);
		}
		ud.hp = hp;
	}

	if (nAttr?.wounds !== undefined) {
		const w = {};
		for (const h of wndsKeys) {
			if (h.startsWith('-=')) continue; // skip key deletion
			const value = nAttr.wounds[h];
			if (value === undefined) continue;
			const type = wndsKeyMap[h];
			if (w[type]) continue;

			const d = new Delta(value, oAttr.wounds[h], type);
			w[type] = d;
			ud._all.push(d);
		}
		ud.wounds = w;
	}

	if (nAttr?.vigor) {
		const v = {};
		for (const h of vigKeys) {
			if (h.startsWith('-=')) continue; // skip key deletion
			const value = nAttr.vigor[h];
			if (value === undefined) continue;
			const type = vigKeyMap[h];
			if (!v[type]) {
				const d = new Delta(value, oAttr.vigor[h], vigKeyMap[h]);
				v[type] = d;
				ud._all.push(d);
			}
		}
		ud.vigor = v;
	}

	if (nAttr?.energyDrain !== undefined) {
		const d = new Delta(nAttr.energyDrain, oAttr.energyDrain, 'ld');
		ud.drain = d;
		ud._all.push(d);
	}

	const nAbl = newD?.abilities, oAbl = oldD.abilities;

	if (nAbl) {
		const tAbl = {};
		for (const ablKey of ablKeys) {
			if (nAbl[ablKey] === undefined) continue;
			for (const ablStat of Object.keys(nAbl[ablKey])) {
				if (!ablSubKeys.includes(ablStat)) continue; // ignore
				if ((nAbl[ablKey][ablStat] ?? 0) === (oAbl[ablKey][ablStat] ?? 0)) continue;
				tAbl[ablKey] ??= {};
				const d = new Delta(nAbl[ablKey][ablStat] ?? 0, oAbl[ablKey][ablStat] ?? 0, ablKey, ablStat);
				tAbl[ablKey][ablStat] = d;
				ud._all.push(d);
			}
		}
		ud.abl = tAbl;
	}

	if (CFG.debug) console.log('HEALTH MONITOR | harvestData:', deepClone(ud));

	return ud;
};

/**
 * @param {Actor} actor
 * @param {object} update Pre-expanded update data.
 * @param {object} context Update context options
 * @param {string} userId User ID
 */
const preUpdateActorEvent = (actor, update, context, userId) => {
	// Ignore non-system updates
	if (!update.system) return;
	// Ignore imports
	if (context.diff === false || context.recursive === false) return;
	// Ignore actors in compendiums
	if (actor.pack) return;

	// Simple check to ignore imports and unsupported actors (basic)
	if (!(actor.system.attributes?.hp?.max > 0)) return;

	if (CFG.debug) console.log('HEALTH MONITOR | preUpdateActor:',
		{ actor, update: deepClone(update), context });

	try {
		const ud = harvestData(actor.system, update.system);
		if (foundry.utils.isEmpty(ud)) {
			if (CFG.debug) console.log('%cHEALTH MONITOR%c | No change detected:',
				CFG.COLORS.main, CFG.COLORS.unset, actor.name, actor.id);
		}
		else {
			printHealthCard(actor, ud);
		}
	}
	catch (err) {
		console.error('HEALTH MONITOR | Hook:preUpdateActor | Error:', err);
	}
};

Hooks.once('init', () => {
	game.settings.register(CFG.module, CFG.SETTINGS.transparencyFlag, {
		name: 'Koboldworks.HealthMonitor.Transparency.Label',
		hint: 'Koboldworks.HealthMonitor.Transparency.Hint',
		type: String,
		default: 'observer',
		choices: {
			owned: 'Koboldworks.HealthMonitor.Transparency.Owned',
			observer: 'Koboldworks.HealthMonitor.Transparency.Observer',
			rollmode: 'Koboldworks.HealthMonitor.Transparency.RollMode',
			public: 'Koboldworks.HealthMonitor.Transparency.Public',
			secret: 'Koboldworks.HealthMonitor.Transparency.Secret',
		},
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.module, 'massiveDamage', {
		name: 'Massive Damage',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.module, 'debug', {
		name: 'Debug',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
		onChange: v => CFG.debug = v,
	});

	CFG.debug = game.settings.get(CFG.module, 'debug');
});

/**
 * Modifies chat message to make them pretty.
 *
 * @param {ChatMessage} cm
 * @param {JQuery} html
 */
const renderChatMessageEvent = (cm, [html]) => {
	const flags = cm.flags?.[CFG.module];
	if (!flags) return;

	const ms = html.querySelector('.koboldworks.health-monitor');
	if (!ms) return;

	const base = html.closest('.chat-message[data-message-id]');

	// Preserve whisper target
	const whisper = base.querySelector('.whisper-to');
	base.title = whisper?.textContent?.trim();
	whisper?.remove();

	// Metadata
	const meta = base?.querySelector('.message-metadata');
	if (!game.user.isGM) {
		if (meta) meta.style.display = 'none';
	}
	else {
		const ts = meta?.querySelector('.message-timestamp');
		if (ts) ts.style.display = 'none';
	}

	base?.classList.add('koboldworks', 'health-monitor', flags.heal ? 'healing' : 'damage');

	// Move contents
	ms.querySelector('.prelude')?.remove();
	const sender = base.querySelector('.message-sender');
	const msgBody = document.createElement('div');
	msgBody.classList.add('prime-message');
	sender?.parentElement?.prepend(msgBody);
	msgBody.append(sender, ...ms.childNodes);
	// Fix spacing
	msgBody.querySelectorAll('span').forEach(el => el.before(' '));
	// Remove things to enforce formatting
	sender?.classList.remove('message-sender');
	sender?.classList.add('healing-recipient');
	base?.querySelector('.message-header')?.classList.remove('message-header');
};

Hooks.on('renderChatMessage', renderChatMessageEvent);

Hooks.once('ready', () => {
	Hooks.on('preUpdateActor', preUpdateActorEvent);

	game.modules.get(CFG.module).api = {

	};

	if (CFG.debug) console.log('%cHEALTH MONITOR%c | DEBUG MODE ENABLED', CFG.COLORS.main, CFG.COLORS.unset);
});
