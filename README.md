# Health Monitor

Logs health changes to in chat log in minimalistic message.

![Chat](./img/screencaps/chat.png)

## Rationale

Forgetful GMs and players not applying healing or damage, accountability, logging, catching typos in health changes, etc.

## Configuration

- Option to control visibility:
  - Owned - Player owned actors
  - Observer - Observed actors
  - Roll mode - Respect selected roll mode
  - Public - Explicit transparency
  - Secret - Explicit secrecy
- Option to skip health changes due to rest.

![Config](./img/screencaps/config.png)

## Limitations

Color blind individuals may have trouble distinguishing the messages by color alone, especially those with protanomaly.

Custom CSS can be used to override things to make it more distinct.

Unlinked token health changes are not announced.

## CSS overrides

Following is example of what is needed for overriding the health monitor colors:

```css
#chat .chat-message.koboldworks.health-monitor.healing {
	background-color: #17863b;
	border-color: #bedc3c;
}
#chat .chat-message.koboldworks.health-monitor.healing.whisper {
	background-color: #386546;
	border-color: #ade0ad;
}
#chat .chat-message.koboldworks.health-monitor.damage {
	background-color: #7b1d35;
	border-color: #ea3e3e;
}
#chat .chat-message.koboldworks.health-monitor.damage.whisper {
	background-color: #61313d;
	border-color: #ff9292;
}
```

## Compatibility

None known.

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/health-monitor/-/releases/permalink/latest/downloads/module.json>

Last Foundry v9 compatible version: <https://gitlab.com/koboldworks/pf1/health-monitor/-/raw/0.2.6/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
