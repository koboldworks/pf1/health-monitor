# Change Log

## 1.2.0

- Fix: Wounds causing invalid messages about changes.

## 1.1.0

- Code refactoring.

## 1.0.0.11

- Fix: Actor import would create odd health monito message.

## 1.0.0.10

- Fix: Odd errors with tokens.
- Fix: Another fix for zero deltas.

## 1.0.0.9

- Fix: Messages for zero deltas.

## 1.0.0.8

- Ignore actors in compendiums.

## 1.0.0.7

- Fix layout being wrong in popped out chat log.

## 1.0.0.6

- Fix for errors with basic actors.

## 1.0.0.4

- Fix card displaying actor name instead of token name.

## 1.0.0.3

- Fix odd behaviour when importing actors (questionable mitigation only).

## 1.0.0.2

- Fix usage of obsolete `CONST.ENTITY_PERMISSIONS`.

## 1.0.0

- Correctly mark as incompatible with Foundry v9

## 1.0.0

- New release mechanism for smaller download & installs sizes.

## 0.2.6

- Internal: Less swapped for SCSS.
- Internal: Bundling via esbuild.
- New: Massive Damage message when damage is suffered in accordance with the optional rule.

## 0.2.5.2

- Fix: Doubled messages for vigor changes.

## 0.2.5.1

- Fix: Key deletion causes errors.

## 0.2.5

- New: Wounds & Vigor support

## 0.2.4.1

- Fix: Hotfix for doubled updates for upcoming PF1 patches (post PF1 0.80.18).

## 0.2.4

- PF1 0.80.16 HP offset compatibility.

## 0.2.3

- Internal: Avoid unnecessary processing.
- Fixed: Respect default actor permission for visibility.
- Changed: Chat message layout improved. Better handling of long character names especially.

## 0.2.2

- Changed: Internal logic was drastically simplified.
- Changed: Foundry 0.7.x support removed for good.
  Anyone still on old FVTT should use the older versions.

## 0.2.1.4 Skip weird "gains 0" statements

## 0.2.1.3 Fix for unlinked tokens in 0.7.x

## 0.2.1.2 Foundry 0.7.x cross-compatibility

## 0.2.1.1

- Fix: Permission checking was not done correctly.

## 0.2.1

- Foundry VTT 0.8 compatibility

## 0.2.0 Unlinked token support

## 0.1.1

- Private messages are distinct
- Slight improvements to underlying format
- Whisper targets are shown in tooltip.

## 0.1.0.1 Hotfix

- Fixes textContent error when Foundry updates timestamps
- Remove unnecessary loud logging

## 0.1.0 Initial release
